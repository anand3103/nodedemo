const path =require('path');
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const auth = require("./auth/routes/auth.route");
//const log = require("logger");

//init app
const app = express();

const dir = '../upload/..';
console.log("path",path.join(__dirname, dir));

app.use(express.static(path.join(__dirname, dir)));

//port number
const port = process.env.PORT || 3000;
console.log("Your port is :", port);

//set port to express
app.set("port", port);

//bodyparser configuration
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(cors());
app.use(bodyParser.json());


process.on('unhandledRejection', (reason) => {
  //log.error('Unhandled Rejection handled in server file Error: ', reason);
  console.log("Unhandeled Exception occured Bro",reason);
  
});

app.use('/api',auth);
//error handeling for api
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});


//assigning port to server.
app.listen(port, () => {
  console.log("your server is start on port: ", port);
});

module.exports = app;
