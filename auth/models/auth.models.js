const pool = require('../../../config/database');
//const bcrypt = require('bcrypt');
const saltRounds = 6;
var Promise = require('promise');
var Userid;
var fs = require('fs');

exports.securelogin = (body) => {
    return new Promise((resolve, reject) => {
    query1 = "call securelogin(?,?)";
    pool.getConnection((err, conn) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        conn.query(query1, [body.email, body.password], (err, row) => {
          if (err) {
            conn.release();
            reject(err);
          } else {
            resolve(row[0]);
            conn.release();
          }
        });
      }
    });
  });
}

exports.getSubUserDetails = (id) => {
    return new Promise((resolve, reject) => {
    query1 = "call get_sub_user_details(?)";
    pool.getConnection((err, conn) => {
      if (err) {
        console.log(err);
        reject(err);
      } else {
        conn.query(query1, [id], (err, row) => {
          if (err) {
            conn.release();
            reject(err);
          } else {
            resolve(row[0]);
            conn.release();
          }
        });
      }
    });
  });
}

exports.getProfileInfo = (id) => {
    return new Promise((resolve, reject) => {
        query1 = "call get_profile_info_by_id(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[id], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        
                        
                        resolve(row);
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.updateProfile = (body) => {
    return new Promise((resolve, reject) => {
        query = "call update_profile_info(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        pool.getConnection((err, conn) => {
            if(err) {
                reject(err);
            }else{
                conn.query(query, 
                    [
                        body.first_name,
                        body.last_name,
                        body.job_title || null,
                        body.work_phone,
                        body.contact_name || null,
                        body.mobile_phone || null,
                        body.emergency_fname || null,
                        body.emergency_lname || null,
                        body.time_zone || null,
                        body.userid,
                        body.emergencyContact || null,
                        body.emergencyContactPhone || null,
                        body.homePhoneNumber || null,
                        body.otherPhoneNumber || null
                        // body.receiveEmailNotifications,
                        // body.receiveSoftwareUpdateEmails,
                        // body.receiveTrainingEmails,
                    ],
                    (err, row) => {
                    if(err){
                        conn.release();
                        reject(err);
                    }else{
                        resolve(JSON.parse(JSON.stringify(row)));
                        conn.release();
                    }
                });
            }
        });
    })
}

// exports.changePassword = (id) => {
//     return new Promise((resolve, reject) => {
//         query = "call change_profile_password()";
//         pool.getConnection((err, conn) => {
//             if(err) {
//                 reject(err);
//             }else{
//                 conn.query(query,[body.userid , body.newpass], (err, row) => {
//                     if(err){
//                         conn.release();
//                         reject(err);
//                     }else{
//                         resolve(JSON.parse(JSON.stringify(row)));
//                         conn.release();
//                     }
//                 });
//             }
//         });
//     })
// }


exports.changePassword = (body) => {
    return new Promise((resolve, reject) => {
        query = "call change_profile_password(?,?)";
        pool.getConnection((err, conn) => {
            if(err) {
                reject(err);
            }else{
                conn.query(query,[
                    body.user_id, 
                    body.hashedPassword
                ], (err, row) => {
                    if(err){
                        conn.release();
                        reject(err);
                    }else{
                        resolve(row[0]);
                        conn.release();
                    }
                });
            }
        });
    })
}


//*************SMTP *********/

exports.getsmtpdetails = () => {
    return new Promise((resolve, reject) => {
        query1 = "call get_smtp_details()";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                let data={};
                conn.query(query1, (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        data['smtpList']=row[0];
                        data['encryptionList']=row[1];                            
                        resolve(data);
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.addsmtpdetails = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call add_update_smtp_details(?,?,?,?,?,?,?,?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.host,
                    body.user_name,
                    body.user_email,
                    body.password,
                    body.port,
                    body.encryption,
                    body.status,
                    body.smtp_id,
                    body.created_by
                ], 
                (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row);
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.getsmtpdetailsbyid = (id) => {
    return new Promise((resolve, reject) => {
        query1 = "call get_smtp_details_by_id(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[id], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row);
                        conn.release(); 
                    }
                });
            }
        });
    })
}


exports.smtpCheckemail = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call check_smtp_email(?, ?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.email,
                    body.smtp_id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {
                        resolve(row[0]);
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.updatesmtpdetails = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call update_smtp_details(?,?,?,?,?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.smtp_host,
                    body.smtp_username,
                    body.smtp_password,
                    body.smtp_post,
                    body.status,
                    body.smtp_id
                ], (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row);
                        conn.release(); 
                    }
                });
            }
        });
    })
}


exports.getactivesmtp = () => {
    return new Promise((resolve, reject) => {
        query1 = "call get_active_smtp()";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1, (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row[0]);
                        conn.release(); 
                    }
                });
            }
        });
    })
}



exports.resetPassword = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call reset_password_update_token(?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.token,
                    body.email
                ],
                     (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row[0]);
                        conn.release(); 
                    }
                });
            }
        });
    })
}

exports.resetpassword = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call password_reset(?,?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.token,
                    body.password
                ],
                     (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row[0]);
                        conn.release(); 
                    }
                });
            }
        });
    })
}


exports.getUserDetailsByResetToken = (body) => {
    return new Promise((resolve, reject) => {
        query1 = "call get_user_details_by_reset_token(?)";
        pool.getConnection((err, conn) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                conn.query(query1,[
                    body.token
                ],
                     (err, row) => {
                    if (err) {
                        conn.release();
                        reject(err);
                    } else {                            
                        resolve(row);
                        conn.release(); 
                    }
                });
            }
        });
    })
}