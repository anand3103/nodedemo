const Joi = require('@hapi/joi');

exports.secureLogin = async (req, res, next) => {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().min(3).max(15).required(),
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json("Error: " + err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};

exports.ValidateSmtpDetailsById = async (req, res, next) => {
    const schema = Joi.object().keys({
        id: Joi.number().integer().required()
    });
    await Joi.validate(req.params, schema, (err, value) => {
        if (err) {
            res.status(400).json("Error: " + err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};

exports.ValidateUpdateProfile = async (req, res, next) => {
    let schema = Joi.object().keys({
        email: Joi.string().email().required(),
        contact_name:Joi.string().allow('', null),
        emergencyContact:Joi.string().allow('', null),
        emergencyContactPhone:Joi.string().allow('', null),
        emergency_fname:Joi.string().allow('', null),
        emergency_lname:Joi.string().allow('', null),
        first_name: Joi.string().required(),
        homePhoneNumber:Joi.string().allow('', null),
        job_title:Joi.string().allow('', null).required(),
        last_name: Joi.string().required(),
        mobile_phone:Joi.string().allow('', null).required(),
        otherPhoneNumber:Joi.string().allow('', null),
        receiveEmailNotifications:Joi.string().allow('', null),
        receiveSoftwareUpdateEmails:Joi.string().allow('', null),
        receiveTrainingEmails:Joi.string().allow('', null),
        time_zone: Joi.string().allow('', null),
        userid: Joi.number().required(),
        work_phone: Joi.string().required()
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json(err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};


exports.ValidateChangePassword = async (req, res, next) => {
    let schema = Joi.object().keys({
        newPassword:Joi.string().required(),
        confirmPassword:Joi.ref('newPassword'),
        user_id: Joi.number().required()
    }).options({ stripUnknown: true});
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json(err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};


exports.ValidateSmtpCheckEmail = async (req, res, next) => {
    let schema = Joi.object().keys({
        email: Joi.string().email().required(),
        smtp_id: Joi.string().required()
    });
    if(req.body.smtp_id =='' || req.body.smtp_id === null)
    {
        schema = Joi.object().keys({
            email: Joi.string().allow('', null).email().required(),
            smtp_id: Joi.string().allow('', null).required()
        });
    }
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json(err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};




exports.ValidateaddUpdateSmtp = async (req, res, next) => {
    let schema = Joi.object().keys({
        encryption: Joi.number().integer().required(),
        host :Joi.string().required(),
        password :Joi.string().required(),
        port: Joi.number().integer().required(),
        smtp_id:Joi.number().integer().required(),
        status : Joi.string().required(),
        user_email : Joi.string().email().required(),
        user_name :Joi.string().required(),
        created_by : Joi.number().integer().required(),
    });
    if(req.body.smtp_id =='' || req.body.smtp_id === null)
    {
        req.body.smtp_id=null;
        schema = Joi.object().keys({
            encryption: Joi.number().integer().required(),
            host :Joi.string().required(),
            password :Joi.string().required(),
            port: Joi.number().integer().required(),
            smtp_id:Joi.number().allow('', null).integer().required(),
            status : Joi.string().required(),
            user_email : Joi.string().email().required(),
            user_name :Joi.string().required(),
            created_by : Joi.number().integer().required(),
        });
    }
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json("Error: " + err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};


exports.ValidateResetToken = async (req, res, next) => {
    let schema = Joi.object().keys({
        token: Joi.string().required()
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json(err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};

exports.ValidateResetPassword = async (req, res, next) => {
    let schema = Joi.object().keys({
        email: Joi.string().email().required(),
    });
    await Joi.validate(req.body, schema, (err, value) => {
        if (err) {
            res.status(400).json(err['details'][0]['message']);
            console.log("Error: ", err['details'][0]['message']);
        } else {
            next();
        }
    });
};




